// Will sit in between a request and a protected route and verify if the request is authorized.
// This middleware function will look for the token in the cookies from the request and then validate it.

const jwt = require('jsonwebtoken');

require('dotenv').config();

const withAuth = function (req, res, next) {
    const token =
        req.body.token ||
        req.query.token ||
        req.headers['x-access-token'] ||
        req.cookies.token;

    if (!token) {
        res.status(401).send('Unauthorized: No token provided');
    } else {
        jwt.verify(token, process.env.MY_SUPER_TOKEN, function (err, decoded) {
            if (err) {
                res.status(401).send('Unauthorized: Invalid token');
            } else {
                req.username = decoded.username;
                next();
            }
        });
    }
}

module.exports = withAuth;