const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChannelSchema = new Schema({
    name: {type: String, required: true, unique: false},
    platform: {type: String, required: true, unique: false},
    gameName: {type: String, required: true, unique: false},
    server: {type: String, required: true, unique: false},
    gameMode: {type: String, required: true, unique: false},
    nbPlayers: {type: Number, required: true, unique: false},
    // startDate: {type: Date, default: Date.now }
});

module.exports = mongoose.model('Channel', ChannelSchema);