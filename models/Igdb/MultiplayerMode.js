const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MultiplayerModeSchema = new Schema({
    name: {type: String, required: true},
    onlineCoopMax: {type: Number, required: true},
    onlineMax: {type: Number, required: true},
});

module.exports = mongoose.model('MultiplayerMode', MultiplayerModeSchema);