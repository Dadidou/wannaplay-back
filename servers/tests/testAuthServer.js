//================DEPENDENCIES===========================================
require('dotenv').config();
const express = require('express');
const app = express();
// tools path = require('path');
const cors = require('cors');
const jwt = require('jsonwebtoken');

//=================MIDDLEWARE============================================
app.use(express.json());
// app.use(cors({credentials: true, origin: 'http://localhost:8889'}));

//===================
let refreshTokens = [];
//===================

//=================ROUTES================================================
app.post('/api/token', (req, res) => {
    const refreshToken = req.body.token;

    if(refreshToken === null) return res.sendStatus(401);
    if(!refreshTokens.includes(refreshToken)) return res.sendStatus(403);

    jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
        if(err) return res.sendStatus(403);
        const accessToken = generateAccessToken({name: user.name});
        res.json({accessToken: accessToken});
    })
});

app.delete('api/logout', (req, res) => {
    console.log(refreshTokens);
    refreshTokens = refreshTokens.filter(token => token !== req.body.token);
    console.log(refreshTokens);
    res.sendStatus(204);
});

app.post('/api/login', (req,res) => {
    const username = req.body.username;
    const user = {name: username};

    const accessToken = generateAccessToken(user);
    const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET)
    refreshTokens.push(refreshToken);
    res.json({accessToken: accessToken, refreshToken: refreshToken});
});

//=================LAUNCH================================================

function generateAccessToken(user) {
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '15s' });
}

const port = process.env.PORT_TESTAUTHSRV || 5110;
app.listen(port);

console.log('App is listening on port ' + port);
