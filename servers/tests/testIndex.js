//================DEPENDENCIES===========================================
require('dotenv').config();
const express = require('express');
const app = express();
// tools path = require('path');
const cors = require('cors');
const jwt = require('jsonwebtoken');

//==============DATA================
const channels = [
    {
        username: 'usr 1',
        channel: 'chan 1'
    },
    {
        username: 'usr 1',
        channel: 'chan 2'
    },
    {
        username: 'usr 1',
        channel: 'chan 3'
    },
    {
        username: 'usr 2',
        channel: 'chan 4'
    },
    {
        username: 'usr 2',
        channel: 'chan 5'
    }
];

//=================MIDDLEWARE============================================
app.use(express.json());
app.use(cors({credentials: true, origin: 'http://localhost:8889'}));

//=================ROUTES================================================
app.get('/api/getChannels', (req,res) => {
    res.json(channels);
});

app.get('/api/getOwnChannels', authenticateToken, (req,res) => {
    res.json(channels.filter(chan => chan.username === req.user.name));
});

//=================LAUNCH================================================
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if (token === null) return res.sendStatus(401);

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {

        if (err) {
            console.log(err);
            return res.sendStatus(403);
        }
        req.user = user;
        next();
    });
}

const port = process.env.PORT_TESTINDEX || 5100;
app.listen(port);

console.log('App is listening on port ' + port);
