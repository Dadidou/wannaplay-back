// --> https://medium.com/@faizanv/authentication-for-your-react-and-express-application-w-json-web-tokens-923515826e0#6563

const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const cors = require('cors');
const withAuth = require('../middlewares/tokenValidation');
const User = require('../models/User');
const app = express();

require('dotenv').config();


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors());            // Allows CORS
app.use(express.static(path.join(__dirname, 'public')));

mongoose.connect(process.env.URI_MONGO, {useNewUrlParser: true}, function (err) {
    if (err) {
        throw err;
    } else {
        console.log(`Successfully connected to ${process.env.URI_MONGO}`);
    }
});

// Catch 404 and forward to error handler 
app.use(function (req, res, next) { 
    next(createError(404)); 
}); 

// Error handler 
app.use(function (err, req, res, next) { 
  
    // Set locals, only providing error 
    // in development 
    res.locals.message = err.message; 
    res.locals.error = req.app.get('env')  
            === 'development' ? err : {}; 
  
    // render the error page 
    res.status(err.status || 500); 
    res.render('error'); 
}); 


//=============================Routes========================================================================

//
app.get('/api/mysupertoken', withAuth, function (req, res) {
    console.log(req);
    res.send('The username is ' + req.username + ', the token is ' + req.cookies.token);
});

// Just check token :
// app.get('/checkToken', withAuth, function(req, res) {
//     res.sendStatus(200);
// });

app.post('/api/register', function (req, res) {
    const {username, password, email} = req.body;

    const user = new User({username, password, email});
    user.save(function (err) {
        if (err) {
            console.log(err);
            res.status(500).send("500 : Error registering new user please try again.");
        } else {
            res.status(200)
                // ----- Data to the front
                .json({newUser: user});
            console.log(res);
        }
    });
});

// Given an username and password, will find a User with the given username and verify that the given password is correct.
// If the password is correct, we will issue a signed token to the requester
app.post('/api/login', function (req, res) {
    const {username, password} = req.body;

    User.findOne({username}, function (err, user) {
        if (err) {
            res.status(500)
                .json({
                    error: '500 : Internal error please try again'
                });
        } else if (!user) {
            res.status(401)
                .json({
                    error: 'Incorrect username or password'
                });
        } else {
            user.isCorrectPassword(password, function (err, same) {
                if (err) {
                    res.status(500)
                        .json({
                            error: '500 : Internal error please try again'
                        });
                } else if (!same) {
                    res.status(401)
                        .json({
                            error: 'Incorrect username or password'
                        });
                } else {
                    // Issue token
                    const payload = {username};
                    const token = jwt.sign(payload, process.env.MY_SUPER_TOKEN, {
                        expiresIn: '1h'
                    });
                    // TODO: Gérer les cookies :
                    // We set it as a cookie and set the httpOnly flag to true.
                    // This method of issuing tokens is ideal for a browser environment because its sets an httpOnly
                    // cookie which helps secure the client from certain vulnerabilities such as XSS (but vulnerable
                    // against CSRF).
                    // res.cookie('token', token, {httpOnly: true})

                    res.status(200)
                        // ----- Data to the front
                        .json({user: user});
                    console.log(res);
                }
            });
        }
    });
});
//=====================================================================================================

app.listen(process.env.PORT_AUTHSRV || 5010);
