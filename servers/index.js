// --> https://www.npmjs.com/package/igdb-api-node

const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');
const mongoose = require('mongoose');
const cors = require('cors');
const Channel = require('../models/Channel');
const withAuth = require('../middlewares/tokenValidation');
const app = express();

require('dotenv').config();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors());            // Allows CORS
app.use(express.static(path.join(__dirname, 'public')));

mongoose.connect(process.env.URI_MONGO, {useNewUrlParser: true}, function (err) {
    if (err) {
        throw err;
    } else {
        console.log(`Successfully connected to ${process.env.URI_MONGO}`);
    }
});

// Catch 404 and forward to error handler 
app.use(function (req, res, next) { 
    next(createError(404)); 
}); 

// Error handler 
app.use(function (err, req, res, next) { 
  
    // Set locals, only providing error 
    // in development 
    res.locals.message = err.message; 
    res.locals.error = req.app.get('env')  
            === 'development' ? err : {}; 
  
    // render the error page 
    res.status(err.status || 500); 
    res.render('error'); 
}); 

//=============================Routes========================================================================

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

// TODO: Route protected with token validation
app.post('/api/channels/new', withAuth, function (req, res) {

    const newChannel = new Channel({
        name: req.body.name,
        platform: req.body.platform,
        gameName: req.body.gameName,
        server: req.body.server,
        gameMode: req.body.gameMode,
        nbPlayers: req.body.nbPlayers,
        // startDate: req.body.startDate
    });

    newChannel.save()
        .then((data) => {
            console.log(data);
        })
        .catch((err) => {
            console.log(err);
        });
});

// TODO: Route protected with token validation
app.get('/api/allChannels', withAuth, function (req, res) {

    Channel.find({}, function (err, channels) {
        if (err) {
            res.status(500)
                .json({
                    error: '500 : Internal error please try again'
                });
        } else {
            res.status(200)
                // ----- Data to the front
                .json({channels: channels});
            console.log(res);

        }
    });

});

//TODO: Route channels/:id ?
// --> On peut faire fetchAllChannelItems pour récup seulement des "channels lights" avec seulement quelques infos utiles (+chanId) pour ChannelCard,
// puis faire getChannelById pour récup des "vrais channels" avec toutes les infos utiles pour ChannelPage

//=====================================================================================================

app.listen(process.env.PORT_INDEX || 5000);
