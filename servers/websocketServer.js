const express = require("express");
const socket = require("socket.io");

// App setup
const PORT = process.env.PORT_WEBSOCKETSRV || 5030;
const app = express();
const server = app.listen(PORT, function () {
    console.log(`Listening on port ${PORT}`);
    console.log(`http://localhost:${PORT}`);
  });

app.use(express.static(path.join(__dirname, 'public')));

//===========================Socket io==========================================================================

const io = socket(server);

io.emit("ping")

io.on("pong", function (socket) {
  console.log("The test is ok !");
});