// --> https://www.npmjs.com/package/igdb-api-node

const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');
const mongoose = require('mongoose');
const cors = require('cors');
const Platform = require('../models/Igdb/Platform');
require('dotenv').config();
const IgdbApiConnector = require('../connectors/IgdbApiConnector')

const app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors());            // Allows CORS
app.use(express.static(path.join(__dirname, 'public')));

mongoose.connect(process.env.URI_MONGO, {useNewUrlParser: true}, function (err) {
    if (err) {
        throw err;
    } else {
        console.log(`Successfully connected to ${process.env.URI_MONGO}`);
    }
});

//=====================================================================================================

app.get('/api/allGames', function (req, res) {

    IgdbApiConnector.get('/games', {
        data: 'fields name, multiplayer_modes; limit 100; sort id;'

    })

});



//=====================================================================================================

app.listen(process.env.PORT_IGDBAPISRV || 5020);
