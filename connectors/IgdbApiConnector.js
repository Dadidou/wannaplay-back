import axios from "axios";

export default axios.create({
    baseURL: process.env.BASE_URL_IGDBAPI_CONNECTOR,
    // withCredentials: false,                 // No CORS
    headers: {
        'user-key': process.env.KEY_IGDBAPI
    }
});